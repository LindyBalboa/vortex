# `vcpkg` Overlay for `vortex`

This is a `vcpkg` ports overlay that is customized for `vortex`.
All `vortex` dependencies can be installed via `vcpkg` using this overlay.

## Usage

To use, pass its path to `vcpkg` using the `--overlay-ports` argument, as below.

```console
vcpkg --overlay-ports=vortex/.vcpkg-overlay install reflexxes
```

The ports overlay may be located anywhere on your system as long as you pass the correct path via `--overlay-ports`.
You may use multiple ports overlays by passing multiple instances of `--overlay-ports`.

## Packages

The overlay provides several new packages and patches several existing packages.

### `backward` (New)

A tool for printing nice stack traces in C++.

### `reflexxes` (New)

The Reflexxes motion library for generating scan patterns.

### `xtensor-python` (New)

Python bindings for `xtensor`.

### `xtensor` (Patch)

An older version of `vcpkg`'s `xtensor` port for compatibility with `xsimd` and `xtensor-python`.
This also patches the CMake configuration scripts to correct flags for `xtensor::optimize` when NVCC is the compiler.
Without this port, `xtensor` breaks CUDA kernel builds.

### `xsimd` (Pin)

An older version of `vcpkg`'s `xsimd` port for compatibility with `xtensor`.
