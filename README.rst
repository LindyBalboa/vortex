Vortex
======

`vortex` is a high-performance C++ library with Python bindings for building real-time OCT engines. It provides:

- modular components out of which an OCT engine is built
- a flexible OCT engine that accommodates most use cases
- extensive Python bindings that interoperate with NumPy and CuPy

`vortex` is designed from the ground up to:

- promote reproducibility in data acquisition and processing
- guarantee synchronization between data streams
- support on-the-fly changes for interactive use cases
- integrate cleanly with user interfaces
- facilitate rapid prototyping with high-level abstractions and sane defaults
- minimize restrictions and assumptions

Documentation
-------------

Documentation is available for `the latest release <https://www.vortex-oct.dev/rel/latest/doc/>`_ and for `specific versions <https://www.vortex-oct.dev/#releases>`_.

Getting Started
---------------

Python
^^^^^^

Install with ``pip``, where the suffix ``-cudaXXY`` denotes a build for CUDA ``XX.Y``.

.. _website: https://www.vortex-oct.dev/#releases

.. code-block:: powershell

    pip install vortex-oct-cuda112 vortex-oct-tools -f https://www.vortex-oct.dev/

Visit the `online documentation <https://www.vortex-oct.dev/rel/latest/doc/getting-started/#python-from-binary-wheel>`_ for more details.

C++
^^^

See the `online documentation <https://www.vortex-oct.dev/rel/latest/doc/getting-started/#c>`_.

License
-------

`vortex` is released under the permissive BSD-3 license to support research and commercial use.
