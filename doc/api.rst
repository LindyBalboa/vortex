Reference
=========

*vortex* provides a complete API in both C++ and Python.
The most flexibility is available through C++, where you can either use the provided components as is or extend the *vortex* template library.
At present, the Python bindings do require several assumptions, especially regarding datatypes, but elimination of this restriction is planned.
It is expect that most will use *vortex* via its Python bindings.

C++
---

* :ref:`Reference <cpp_reference>`


Python
------

* :ref:`Reference <py_reference>`
* :ref:`genindex`
* :ref:`modindex`

.. toctree::
   :hidden:

   C++ <api/cpp>
   Python <api/py>
