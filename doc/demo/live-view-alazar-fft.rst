.. _demo/alazar-fft:

Live Alazar FFT Acquisition
===========================

.. warning::

    This document is under construction.

This demo illustrates how to combine :class:`~vortex.acquire.AlazarFFTAcquisition` with :class:`~vortex.process.CopyProcessor` to process OCT data with an Alazar on-board FPGA.
This requires a somewhat low-level understanding of the Alazar FPGA and review of the `Alazar DSP documentation`_ is recommended.

.. _`Alazar DSP documentation`: https://docs.alazartech.com/ats-sdk-user-guide/latest/alazardsp.html
