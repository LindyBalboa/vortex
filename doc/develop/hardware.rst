Hardware
========

.. warning::

    This document is under construction.

This document discusses how to support new hardware with *vortex*.

#.  Create a new module target to contain the driver.
#.  Write the driver classes.
#.  Create a new binding module target to contain the bindings.
#.  Write the bindings.
#.  Consider contributing your driver and/or bindings to the open-source project.
    Reach out via the `contact form <https://www.vortex-oct.dev/#contact>`_.
