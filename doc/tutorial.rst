Tutorial
========

This tutorial walks through the development of a custom graphical OCT application using *vortex*.
They illustrate how to accomplish common tasks with *vortex* and showcase the various options that *vortex* provides.

.. toctree::
   :maxdepth: 2

   tutorial/acquire
   tutorial/process
   tutorial/format
   tutorial/scan
   tutorial/storage
   tutorial/display
   tutorial/dynamic
