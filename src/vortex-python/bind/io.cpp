#include <vortex-python/bind/common.hpp>

#include <vortex/io.hpp>

static void bind_null(py::module& m) {
    using T = vortex::io::null_io_t<vortex::io::null_config_t>;

    {
        using C = T::config_t;
        CLS_VAL(NullConfig);

        c.def(py::init());

        FXN(validate);

        SHALLOW_COPY();
    }

    {
        using C = T;
        CLS_PTR(NullIO);

        c.def(py::init());

        RO_ACC(config);

        FXN_GIL(initialize);

        FXN_GIL(prepare);
        FXN_GIL(start);
        FXN_GIL(stop);
    }
}

void bind_io(py::module& root) {
    auto m = root.def_submodule("io");

    bind_null(m);
}
