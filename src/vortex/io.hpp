#pragma once

#include <vortex/io/null.hpp>

namespace vortex {

    using null_io_config_t = io::null_config_t;

    using null_io_t = io::null_io_t<null_io_config_t>;

}

#if defined(VORTEX_ENABLE_DAQMX)

#include <vortex/io/daqmx.hpp>

namespace vortex {

    using daqmx_config_t = io::daqmx_config_t<io::default_channel_t>;

    using daqmx_io_t = io::daqmx_io_t<daqmx_config_t>;

}

#endif