#pragma once

#include <xtensor/xstorage.hpp>

#include <fmt/format.h>

namespace vortex {

    namespace detail {

        template<typename T>
        class tensor_impl_t {
        public:

            tensor_impl_t() {};
            virtual ~tensor_impl_t() {};

            tensor_impl_t(const tensor_impl_t&) = delete;
            tensor_impl_t& operator=(const tensor_impl_t&) = delete;

            tensor_impl_t(tensor_impl_t&& o) {
                *this = std::move(o);
            }
            tensor_impl_t& operator=(tensor_impl_t&& o) {
                clear();

                std::swap(_ptr, o._ptr);
                std::swap(_shape, o._shape);
                std::swap(_stride, o._stride);

                return *this;
            }

            template<typename shape_t>
            void resize(const shape_t& s, bool shrink = true) {
                _resize(s, shrink);
            }
            template<typename U>
            void resize(std::initializer_list<U>&& s, bool shrink = true) {
                _resize(std::forward<std::initializer_list<U>>(s), shrink);
            }

            void shrink() {
                _resize(_shape, true);
            }

            void clear() {
                resize(std::array<size_t, 0>{}, true);
            }

            auto data() {
                return _ptr;
            }
            const auto data() const {
                return _ptr;
            }

            auto count() const {
                if (_shape.empty()) {
                    return 0ULL;
                } else {
                    return std::accumulate(_shape.begin(), _shape.end(), 1ULL, std::multiplies());
                }
            }
            virtual size_t underlying_count() const = 0;

            auto size_in_bytes() const {
                return count() * sizeof(T);
            }
            auto underlying_size_in_bytes() const {
                return underlying_count() * sizeof(T);
            }

            auto dimension() const {
                return _shape.size();
            }

            const auto& shape() const {
                return _shape;
            }
            auto shape(size_t idx) const {
                return _shape[idx];
            }

            const auto& stride() const {
                return _stride;
            }
            auto stride(size_t idx) const {
                return _stride[idx];
            }

            auto stride_in_bytes(size_t idx) const {
                return stride(idx) * sizeof(T);
            }
            auto stride_in_bytes() const {
                std::vector<size_t> s(dimension());
                for (size_t i = 0; i < dimension(); i++) {
                    s[i] = stride_in_bytes(i);
                }
                return s;
            }

            auto valid() const {
                return count() > 0;
            }

        protected:

            virtual void _allocate(size_t n) = 0;

            template<typename shape_t>
            void _resize(const shape_t& s, bool shrink) {
                _shape.assign(s.begin(), s.end());
                _stride.resize(_shape.size());

                // NOTE: do not use xt::compute_strides(...) because it sets stride to 0 for singleton dimensions
                auto n = dense_stride(_shape, _stride);
                if (n > underlying_count() || shrink) {
                    _allocate(_shape.empty() ? 0 : n);
                }
            }

            T* _ptr;

            xt::svector<size_t> _shape;
            xt::svector<ptrdiff_t> _stride;

        };
    }

}
