/** \rst

    CUDA kernels to support CUDA OCT processor

 \endrst */

// #include <cub/device/device_segmented_reduce.cuh>
// #include <cub/iterator/transform_input_iterator.cuh>

#include <vortex/driver/cuda/types.hpp>
#include <vortex/driver/cuda/runtime.hpp>
#include <vortex/driver/cuda/kernels.cuh>

#define INDEX_ENCODE_2D() \
    auto threads = cuda::kernel::threads_from_shape(out.shape.y, out.shape.x); \
    auto blocks = cuda::kernel::blocks_from_threads(threads, out.shape.y, out.shape.x);

#define INDEX_DECODE_2D() \
    auto record_idx = blockIdx.y * blockDim.y + threadIdx.y; \
    auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x; \
    if (record_idx >= out.shape.x || sample_idx >= out.shape.y) { \
        return; \
    }

// NOTE: no namespace shorthand here so NVCC can compile this file
namespace vortex {
    namespace process {
        namespace detail {

            //
            // resample
            //

            template<typename in_t, typename out_t, typename index_t, typename float_t>
            __global__
            static void _resample(
                cuda::strided_t<const index_t, 1> before_index,  cuda::strided_t<const index_t, 1> after_index,
                cuda::strided_t<const float_t, 1> before_weight, cuda::strided_t<const float_t, 1> after_weight,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<out_t, 2> out
            ) {
                INDEX_DECODE_2D();

                // look up bounding samples
                auto offset = before_index.offset(sample_idx);
                auto& before = in(record_idx, before_index[offset]);
                auto& after = in(record_idx, after_index[offset]);

                // perform interpolation
                out(record_idx, sample_idx) = cuda::kernel::round_clip_cast<out_t>(before_weight[offset] * before + after_weight[offset] * after);
            }

            template<typename in_t, typename out_t, typename index_t, typename float_t>
            void _resample_internal(
                const cuda::stream_t& stream, 
                const cuda::strided_t<const index_t, 1>& before_index, const cuda::strided_t<const index_t, 1>& after_index,
                const cuda::strided_t<const float_t, 1>& before_weight, const cuda::strided_t<const float_t, 1>& after_weight,
                const cuda::strided_t<const in_t, 2>& in,
                const cuda::strided_t<out_t, 2>& out
            ) {
                INDEX_ENCODE_2D();

                _resample<<<blocks, threads, 0, stream.handle()>>>(before_index, after_index, before_weight, after_weight, in, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "resample kernel launch failed");
            }

            void resample(
                const cuda::stream_t& stream,
                const cuda::strided_t<const uint16_t, 1>& before_index, const cuda::strided_t<const uint16_t, 1>& after_index,
                const cuda::strided_t<const float, 1>& before_weight, const cuda::strided_t<const float, 1>& after_weight,
                const cuda::strided_t<const uint16_t, 2>& in,
                const cuda::strided_t<float, 2>& out
            ) {
                _resample_internal(stream, before_index, after_index, before_weight, after_weight, in, out);
            }
            void resample(
                const cuda::stream_t& stream,
                const cuda::strided_t<const uint16_t, 1>& before_index, const cuda::strided_t<const uint16_t, 1>& after_index,
                const cuda::strided_t<const float, 1>& before_weight, const cuda::strided_t<const float, 1>& after_weight,
                const cuda::strided_t<const float, 2>& in,
                const cuda::strided_t<float, 2>& out
            ) {
                _resample_internal(stream, before_index, after_index, before_weight, after_weight, in, out);
            }

            //
            // remove average
            //

            //struct transpose_op_t {
            //    __host__ __device__
            //        transpose_op_t(size_t rows, size_t cols) : _rows(rows), _cols(cols) {
            //
            //    }
            //
            //    __host__ __device__ __forceinline__
            //        size_t operator()(const size_t& i) const {
            //        return (i % _rows) * _cols + (i / _rows);
            //    }
            //
            //    size_t _rows, _cols;
            //};

            __global__
            static void _compute_average(
                cuda::strided_t<const uint16_t, 2> average_record_buffer,
                cuda::strided_t<float, 1> average_record
            ) {
                // A-scans run along the rows
                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;

                // check valid source coordinates
                if (sample_idx >= average_record_buffer.shape.y) {
                    return;
                }

                // perform averaging
                float out = 0.0f;
                for (auto record_idx = 0; record_idx < average_record_buffer.shape.x; record_idx++) {
                    out += average_record_buffer(record_idx, sample_idx);
                }

                // store result
                average_record(sample_idx) = out / average_record_buffer.shape.x;
            }

//            __global__
//                static void _normalize_average(size_t samples_per_record, size_t average_record_count, float* average_record) {
//                // A-scans run along the rows
//                auto sample_idx = blockIdx.x * blockDim.x + threadIdx.x;
//
//                // check valid source coordinates
//                if (sample_idx >= samples_per_record) {
//                    return;
//                }
//
//                // normalize
//                average_record[sample_idx] /= average_record_count;
//            }
//
//            size_t compute_average_internal_storage_size(size_t samples_per_record, const uint16_t* average_record_buffer, size_t average_record_count, float* average_record) {
//                transpose_op_t transpose_op(average_record_count, samples_per_record);
//                cub::TransformInputIterator<size_t, transpose_op_t, const uint16_t*> input_it(average_record_buffer, transpose_op);
//
//                std::vector<size_t> offsets;
//                offsets.resize(samples_per_record + 1);
//                for (size_t i = 0; i < offsets.size(); i++) {
//                    offsets[i] = i * average_record_count;
//                }
//
//                size_t internal_storage_size_in_bytes;
//                cub::DeviceSegmentedReduce::Sum(nullptr, internal_storage_size_in_bytes, input_it, average_record, samples_per_record, offsets.data(), offsets.data() + 1);
//
//                return internal_storage_size_in_bytes;
//            }
//            void compute_average(const cuda::stream_t& stream, const uint8_t* internal_storage, size_t samples_per_record, const uint16_t* average_record_buffer, size_t average_record_count, float* average_record) {
//                cudaError_t error;
//
//                transpose_op_t transpose_op(average_record_count, samples_per_record);
//                cub::TransformInputIterator<size_t, transpose_op_t, const uint16_t*> input_it(average_record_buffer, transpose_op);
//
//                std::vector<size_t> offsets;
//                offsets.resize(samples_per_record + 1);
//                for (size_t i = 0; i < offsets.size(); i++) {
//                    offsets[i] = i * average_record_count;
//                }
//
//                // compute the sum
//                size_t internal_storage_size_in_bytes = internal_storage.size_in_bytes();
//                cub::DeviceSegmentedReduce::Sum((void*)internal_storage, internal_storage_size_in_bytes, input_it, average_record, samples_per_record, offsets.data(), offsets.data() + 1, stream.handle(), true);
//#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
//                cudaDeviceSynchronize();
//#endif
//                error = cudaGetLastError();
//                cuda::detail::handle_error(error, "sum kernel launch failed");
//
//                auto threads = cuda::kernel::threads_from_shape(samples_per_record);
//                auto blocks = cuda::kernel::blocks_from_threads(threads, samples_per_record);
//
//                _normalize_average<<<blocks, threads, 0, stream.handle()>>>(samples_per_record, average_record_count, average_record);
//#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
//                cudaDeviceSynchronize();
//#endif
//                error = cudaGetLastError();
//                cuda::detail::handle_error(error, "normalize kernel launch failed");
//            }
            void compute_average(
                const cuda::stream_t& stream,
                const cuda::strided_t<const uint16_t, 2>& average_record_buffer,
                const cuda::strided_t<float, 1>& average_record
            ) {
                auto threads = cuda::kernel::threads_from_shape(average_record.shape);
                auto blocks = cuda::kernel::blocks_from_threads(threads, average_record.shape);

                _compute_average<<<blocks, threads, 0, stream.handle()>>>(average_record_buffer, average_record);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "compute average kernel launch failed");
            }

            template<typename in_t>
            __global__
            static void _subtract_average(
                cuda::strided_t<const float, 1> average_record,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<float, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform average subtraction
                out(record_idx, sample_idx) = in(record_idx, sample_idx) - average_record(sample_idx);
            }
            template<typename in_t>
            __global__
            static void _subtract_average_ss(
                cuda::strided_t<const float, 1> average_record,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<float, 2> out
            ) {
                INDEX_DECODE_2D();

                auto offset = in.offset(record_idx, sample_idx);

                // perform average subtraction
                out[offset] = in[offset] - average_record(sample_idx);
            }

            template<typename in_t>
            static void _subtract_average_internal(
                const cuda::stream_t& stream,
                const cuda::strided_t<const float, 1>& average_record,
                const cuda::strided_t<const in_t, 2>& in,
                const cuda::strided_t<float, 2>& out
            ) {
                INDEX_ENCODE_2D();

                if (in.stride == out.stride) {
                    _subtract_average_ss<<<blocks, threads, 0, stream.handle()>>>(average_record, in, out);
                } else {
                    _subtract_average<<<blocks, threads, 0, stream.handle()>>>(average_record, in, out);
                }
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "subtract average kernel launch failed");
            }
            void subtract_average(
                const cuda::stream_t& stream,
                const cuda::strided_t<const float, 1>& average_record,
                const cuda::strided_t<const uint16_t, 2>& in,
                const cuda::strided_t<float, 2>& out
            ) {
                _subtract_average_internal(stream, average_record, in, out);
            }

            //
            // complex filter
            //

            template<typename in_t>
            __global__
            static void _complex_filter(
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<const cuFloatComplex, 1> filter,
                cuda::strided_t<cuFloatComplex, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform filtering
                auto input_offset = in.offset(record_idx, sample_idx);
                auto output_offset = out.offset(record_idx, sample_idx);
                out[output_offset].x = filter(sample_idx).x * in[input_offset];
                out[output_offset].y = filter(sample_idx).y * in[input_offset];
            }
            template<typename in_t>
            __global__
            static void _complex_filter_ss(
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<const cuFloatComplex, 1> filter,
                cuda::strided_t<cuFloatComplex, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform filtering
                auto offset = in.offset(record_idx, sample_idx);
                out[offset].x = filter(sample_idx).x * in[offset];
                out[offset].y = filter(sample_idx).y * in[offset];
            }
            template<typename in_t>
            void complex_filter_internal(
                const cuda::stream_t& stream,
                const cuda::strided_t<const in_t, 2>& in,
                const cuda::strided_t<const cuFloatComplex, 1>& filter,
                const cuda::strided_t<cuFloatComplex, 2>& out
            ) {
                INDEX_ENCODE_2D();

                _complex_filter<<<blocks, threads, 0, stream.handle()>>>(in, filter, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "complex filter kernel launch failed");
            }
            void complex_filter(
                const cuda::stream_t& stream,
                const cuda::strided_t<const uint16_t, 2>& in,
                const cuda::strided_t<const cuFloatComplex, 1>& filter,
                const cuda::strided_t<cuFloatComplex, 2>& out
            ) {
                complex_filter_internal(stream, in, filter, out);
            }
            void complex_filter(
                const cuda::stream_t& stream,
                const cuda::strided_t<const float, 2>& in,
                const cuda::strided_t<const cuFloatComplex, 1>& filter,
                const cuda::strided_t<cuFloatComplex, 2>& out
            ) {
                complex_filter_internal(stream, in, filter, out);
            }

            //
            // cast
            //

            template<typename in_t>
            __global__
            static void _cast(
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<cuFloatComplex, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform casting
                auto output_offset = out.offset(record_idx, sample_idx);
                out[output_offset].x = float(in(record_idx, sample_idx));
                out[output_offset].y = 0.0f;
            }
            template<typename in_t>
            __global__
            static void _cast_ss(
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<cuFloatComplex, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform casting
                auto offset = in.offset(record_idx, sample_idx);
                out[offset].x = float(in[offset]);
                out[offset].y = 0.0f;
            }
            template<>
            __global__
            void _cast(
                cuda::strided_t<const cuFloatComplex, 2> in,
                cuda::strided_t<cuFloatComplex, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform copying
                out(record_idx, sample_idx) = in(record_idx, sample_idx);
            }
            template<>
            __global__
            void _cast_ss(
                cuda::strided_t<const cuFloatComplex, 2> in,
                cuda::strided_t<cuFloatComplex, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform copying
                auto offset = in.offset(record_idx, sample_idx);
                out[offset] = in[offset];
            }

            template<typename in_t>
            static void _cast_internal(
                const cuda::stream_t& stream,
                const cuda::strided_t<const in_t, 2>& in,
                const cuda::strided_t<cuFloatComplex, 2>& out
            ) {
                INDEX_ENCODE_2D();

                if (in.stride == out.stride) {
                    _cast_ss<<<blocks, threads, 0, stream.handle()>>>(in, out);
                } else {
                    _cast<<<blocks, threads, 0, stream.handle()>>>(in, out);
                }
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "cast kernel launch failed");
            }
            void cast(const cuda::stream_t& stream, const cuda::strided_t<const uint16_t, 2>& in, const cuda::strided_t<cuFloatComplex, 2>& out) {
                _cast_internal(stream, in, out);
            }
            void cast(const cuda::stream_t& stream, const cuda::strided_t<const float, 2>& in, const cuda::strided_t<cuFloatComplex, 2>& out) {
                _cast_internal(stream, in, out);
            }
            void cast(const cuda::stream_t& stream, const cuda::strided_t<const cuFloatComplex, 2>& in, const cuda::strided_t<cuFloatComplex, 2>& out) {
                _cast_internal(stream, in, out);
            }

            //
            // copy
            //

            template<typename T>
            __global__
            static void _copy(
                cuda::strided_t<const T, 2> in,
                cuda::strided_t<T, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform copy
                out(record_idx, sample_idx) = in(record_idx, sample_idx);
            }

            template<typename T>
            static void _copy_internal(
                const cuda::stream_t& stream,
                const cuda::strided_t<const T, 2>& in,
                const cuda::strided_t<T, 2>& out
            ) {
                INDEX_ENCODE_2D();

                _copy<<<blocks, threads, 0, stream.handle()>>>(in, out);
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "copy kernel launch failed");
            }
            void copy(const cuda::stream_t& stream, const cuda::strided_t<const uint16_t, 2>& in, const cuda::strided_t<uint16_t, 2>& out) {
                _copy_internal(stream, in, out);
            }
            void copy(const cuda::stream_t& stream, const cuda::strided_t<const float, 2>& in, const cuda::strided_t<float, 2>& out) {
                _copy_internal(stream, in, out);
            }
            void copy(const cuda::stream_t& stream, const cuda::strided_t<const int8_t, 2>& in, const cuda::strided_t<int8_t, 2>& out) {
                _copy_internal(stream, in, out);
            }

            //
            // log abs normalize
            //

            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _log10_normalize(
                factor_t prefactor, factor_t postfactor,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<out_t, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform abs normalize
                out(record_idx, sample_idx) = cuda::kernel::round_clip_cast<out_t>(postfactor * log10(prefactor * cuda::kernel::abs(in(record_idx, sample_idx))));
            }
            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _log10_normalize_ss(
                factor_t prefactor, factor_t postfactor,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<out_t, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform abs normalize
                auto offset = in.offset(record_idx, sample_idx);
                out[offset] = cuda::kernel::round_clip_cast<out_t>(postfactor * log10(prefactor * cuda::kernel::abs(in[offset])));
            }
            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _sqr_normalize(
                factor_t prefactor,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<out_t, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform square normalize
                out(record_idx, sample_idx) = cuda::kernel::round_clip_cast<out_t>(cuda::kernel::sqr(prefactor * cuda::kernel::abs(in(record_idx, sample_idx))));
            }
            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _sqr_normalize_ss(
                factor_t prefactor,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<out_t, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform square normalize
                auto offset = in.offset(record_idx, sample_idx);
                out[offset] = cuda::kernel::round_clip_cast<out_t>(cuda::kernel::sqr(prefactor * cuda::kernel::abs(in[offset])));
            }
            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _abs_normalize(
                factor_t prefactor,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<out_t, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform absolute value normalize
                out(record_idx, sample_idx) = cuda::kernel::round_clip_cast<out_t>(prefactor * cuda::kernel::abs(in(record_idx, sample_idx)));
            }
            template<typename in_t, typename out_t, typename factor_t>
            __global__
            static void _abs_normalize_ss(
                factor_t prefactor,
                cuda::strided_t<const in_t, 2> in,
                cuda::strided_t<out_t, 2> out
            ) {
                INDEX_DECODE_2D();

                // perform absolute value normalize
                auto offset = in.offset(record_idx, sample_idx);
                out[offset] = cuda::kernel::round_clip_cast<out_t>(cuda::kernel::abs(prefactor * cuda::kernel::abs(in[offset])));
            }

            template<typename in_t, typename out_t, typename factor_t>
            static void _abs_normalize_internal(
                const cuda::stream_t& stream,
                factor_t factor,
                bool enable_log10, bool enable_square,
                const cuda::strided_t<const in_t, 2>& in,
                const cuda::strided_t<out_t, 2>& out
            ) {
                INDEX_ENCODE_2D();

                if (in.stride == out.stride) {
                    if (enable_log10) {
                        _log10_normalize_ss<<<blocks, threads, 0, stream.handle()>>>(factor, enable_square ? 20.0f : 10.0f, in, out);
                    }
                    else if (enable_square) {
                        _sqr_normalize_ss<<<blocks, threads, 0, stream.handle()>>>(factor, in, out);
                    } else {
                        _abs_normalize_ss<<<blocks, threads, 0, stream.handle()>>>(factor, in, out);
                    }
                } else {
                    if (enable_log10) {
                        _log10_normalize<<<blocks, threads, 0, stream.handle()>>>(factor, enable_square ? 20.0f : 10.0f, in, out);
                    }
                    else if (enable_square) {
                        _sqr_normalize<<<blocks, threads, 0, stream.handle()>>>(factor, in, out);
                    } else {
                        _abs_normalize<<<blocks, threads, 0, stream.handle()>>>(factor, in, out);
                    }                
                }
                
#if defined(VORTEX_SERIALIZE_CUDA_KERNELS)
                cudaDeviceSynchronize();
#endif
                cudaError_t error = cudaGetLastError();
                cuda::detail::handle_error(error, "abs2 normalize kernel launch failed");
            }

#define _DECLARE(factor_t, in_t, out_t) \
    void abs_normalize( \
        const cuda::stream_t& stream, \
        factor_t factor, \
        bool enable_log10, bool enable_square, \
        const cuda::strided_t<const in_t, 2>& in, \
        const cuda::strided_t<out_t, 2>& out \
    ) { \
        _abs_normalize_internal(stream, factor, enable_log10, enable_square, in, out); \
    }

            _DECLARE(float, uint16_t, float);
            _DECLARE(float, float, float);
            _DECLARE(float, cuFloatComplex, float);
            _DECLARE(float, uint16_t, int8_t);
            _DECLARE(float, float, int8_t);
            _DECLARE(float, cuFloatComplex, int8_t);
#undef _DECLARE

            // NOTE: not yet ready to support double for the other functions but there is no reason it cannot be done
            //_DECLARE(double, uint16_t, double);
            //_DECLARE(double, double, double);
            //_DECLARE(double, cuDoubleComplex, double);
            //_DECLARE(double, uint16_t, int8_t);
            //_DECLARE(double, double, int8_t);
            //_DECLARE(double, cuDoubleComplex, int8_t);

        }
    }
}
