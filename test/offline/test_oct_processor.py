import pytest

import numpy as np

def _round_clip_cast(xp, data, dtype):
    info = xp.iinfo(dtype)
    return data.round().clip(info.min, info.max).astype(dtype)

@pytest.fixture
def cpu_processor():
    try:
        from vortex.process import CPUProcessor
    except ImportError:
        pytest.skip('CPU processor not supported')

    return (CPUProcessor(), np)

@pytest.fixture
def cuda_processor():
    try:
        from vortex.process import CUDAProcessor
    except ImportError:
        pytest.skip('CUDA processor not supported')

    cp = pytest.importorskip('cupy')

    if int(cp.cuda.Device().compute_capability) < 35:
        pytest.skip('GPU compute capability is insufficient')

    return (CUDAProcessor(), cp)

@pytest.fixture
def samples_per_record():
    return 1376

@pytest.fixture
def ascans_per_block():
    return 250

CHANNEL_SETUPS = [(1, 0, False), (2, 0, False), (2, 1, False), (2, 0, True), (2, 1, True)]
@pytest.fixture(params=CHANNEL_SETUPS, ids=[f'channels_per_sample={cps}-channel={c}-preindex={i}' for (cps, c, i) in CHANNEL_SETUPS])
def channel_setup(request):
    return request.param

@pytest.fixture(params=[100, 250])
def average_window(request):
    return request.param

@pytest.fixture(params=['cpu_processor', 'cuda_processor'])
def scenario(request, samples_per_record, ascans_per_block, channel_setup):
    (proc, xp) = request.getfixturevalue(request.param)
    (channels_per_sample, channel, preindex) = channel_setup

    cfg = proc.config
    cfg.channels_per_sample = channels_per_sample
    cfg.samples_per_record = samples_per_record
    cfg.ascans_per_block = ascans_per_block
    cfg.channel = 0 if preindex else channel
    cfg.enable_ifft = False
    cfg.enable_log10 = False
    cfg.enable_square = False

    xp.random.seed(1234)
    spectra = xp.random.randint(0, 32767, cfg.input_shape).astype(xp.uint16)
    ascans = xp.random.randint(-128, 127, cfg.output_shape).astype(xp.int8)

    return (xp, proc, cfg, spectra, ascans, channel, preindex)

def test_copy(scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    ref = xp.abs(spectra[..., (channel,)])
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_average(average_window, scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    cfg.average_window = average_window
    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    data = spectra[..., (channel,)]
    if average_window > 0:
        data = data - xp.mean(data[-average_window:], axis=0)
    else:
        ref = data
    ref = xp.abs(data)
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_resample(scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    rs = np.arange(0, spectra.shape[1], 2)
    cfg.resampling_samples = rs
    # reduce output buffer to match
    ascans = ascans[:, rs, :].copy()

    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    ref = xp.abs(spectra[:, rs][..., (channel,)])
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_filter(scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    sf = np.random.random((spectra.shape[1]))
    cfg.spectral_filter = sf

    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    ref = xp.abs(spectra[..., (channel,)] * xp.asanyarray(sf[None, :, None]))
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_ifft(scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    cfg.enable_ifft = True
    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    ref = xp.abs(xp.fft.ifft(spectra[..., (channel,)].astype(float), axis=1))
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_log10(scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    cfg.enable_log10 = True
    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    ref = 10 * xp.log10(spectra[..., (channel,)].astype(float) + xp.finfo(float).eps)
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_square(scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    cfg.enable_square = True
    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    ref = spectra[..., (channel,)].astype(float)**2
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

def test_log10_square(scenario):
    (xp, proc, cfg, spectra, ascans, channel, preindex) = scenario

    cfg.enable_log10 = True
    cfg.enable_square = True
    proc.initialize(cfg)

    if preindex:
        proc.next(spectra[..., channel], ascans)
    else:
        proc.next(spectra, ascans)

    ref = 20 * xp.log10(spectra[..., (channel,)].astype(float) + xp.finfo(float).eps)
    xp.testing.assert_allclose(ascans, _round_clip_cast(xp, ref, xp.int8), atol=1)

if __name__ == '__main__':
    pytest.main()
